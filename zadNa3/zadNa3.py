import matplotlib
import matplotlib.pyplot as plt
from matplotlib import rc
import numpy as np
from matplotlib import colors
import colorsys 

#konwerter: nie trzeba implementowaÄ samemu, moĹźna wykorzystaÄ funkcjÄ z bilbioteki
def hsv2rgb(h, s, v):
    #TODO
    return colorsys.hsv_to_rgb(h, s, v)

# poniĹźej znajdujÄ
# siÄ funkcje modelujÄ
#ce kolejne gradienty z zadania.
# v to pozycja na osi ox: v jest od 0 do 1. Zewnetrzna funkcja wywoĹuje te metody podajÄ
#c
# rĂłĹźne v i oczekujÄ
#c trĂłjki RGB bÄ
#dĹş HSV reprezentujÄ
#cej kolor. Np. (0,0,0) w RGB to kolor czarny. 
# NaleĹźy uwikĹaÄ v w funkcjÄ modelujÄ
#cÄ
# kolor. W tym celu dla kolejnych gradientĂłw trzeba przyjÄ
#Ä 
# sobie jakieĹ punkty charakterystyczne,
# np. widzimy, Ĺźe po lewej stronie (dla v = 0) powinien byÄ kolor zielony a w Ĺrodku niebieski (dla v = 0.5),
# a wszystkie punkty pomiÄdzy naleĹźy interpolowaÄ liniowo (proporcjonalnie). 

def gradient_rgb_bw(v):
    #TODO
    return (v, v, v)


def gradient_rgb_gbr(v):
    #TODO
    r = 0
    g = 0
    b = 0
    if( v < 1/2):
        g = 1 - v*2
        b = v*2
    if( v > 1/2 ):
        b = 2 - v*2
        r = -1 -v*(-2)
    return (r, g, b)


def gradient_rgb_gbr_full(v):
    #TODO
    r = 0
    g = 0
    b = 0
    if( v < 1/4):#00FF00 -> 00FFFF
        r = 0
        g = 1 
        b = v*4
    elif( v < 2/4):#00FFFF -> 0000FF
        r = 0
        g = 2 - v*4
        b = 1
    elif( v < 3/4):#0000FF -> FF00FF
        r = -2 + v*4
        g = 0
        b = 1
    else: #FF00FF -> FF0000
        r = 1
        g = 0
        b = 4 - v*4
   
    return (r, g, b)


def gradient_rgb_wb_custom(v):
    #TODO
    r = 0
    g = 0
    b = 0
    if v < 1/6:#ffffff -> ff00ff
        r = 1
        g = 1 - v*6 
        b = 1
    elif v < 2/6:#ff00ff -> 0000ff
        r = 2 - v*6
        g = 0
        b = 1
    elif v < 3/6:#0000ff -> 00ffff
        r = 0
        g = -2 + v*6
        b = 1
    elif v < 4/6:#00ffff -> 00ff00
        r = 0
        g = 1
        b = 4 - v*6
    elif( v < 5/6):#00ff00 -> ffff00
        r = -4 + v*6
        g = 1
        b = 0
    elif( v < 1):#ffff00 -> ff0000
        r = 1
        g = 6 - v*6
        b = 0
    
    return (r, g, b)


def gradient_hsv_bw(v):
    #TODO
    h = 0
    s = 0
    va = v
    return hsv2rgb(h, s, va)


def gradient_hsv_gbr(v):
    #TODO
    h = 1/3 + v*2/3
    s = 1
    va = 1
    return hsv2rgb(h, s, va)

def gradient_hsv_unknown(v):
    #TODO
    h = 4/3 - v*2/6
    s = 0.5
    va = 1
    return hsv2rgb(h, s, va)


def gradient_hsv_custom(v):
    #TODO
    h = v
    s = 1 - v
    va = 1
    return hsv2rgb(h, s, va)

def plot_color_gradients(gradients, names):
    # For pretty latex fonts (commented out, because it does not work on some machines)
    #rc('text', usetex=True) 
    #rc('font', family='serif', serif=['Times'], size=10)
    rc('legend', fontsize=10)

    column_width_pt = 400         # Show in latex using \the\linewidth
    pt_per_inch = 72
    size = column_width_pt / pt_per_inch

    fig, axes = plt.subplots(nrows=len(gradients), sharex=True, figsize=(size, 0.75 * size))
    fig.subplots_adjust(top=1.00, bottom=0.05, left=0.25, right=0.95)


    for ax, gradient, name in zip(axes, gradients, names):
        # Create image with two lines and draw gradient on it
        numOfLines = 2
        img = np.zeros((numOfLines, 1024, 3))
        for i, v in enumerate(np.linspace(0, 1, 1024)):
            img[:numOfLines, i] = gradient(v)

        im = ax.imshow(img, aspect='auto')
        im.set_extent([0, 1, 0, 1])
        ax.yaxis.set_visible(False)

        pos = list(ax.get_position().bounds)
        x_text = pos[0] - 0.25
        y_text = pos[1] + pos[3]/2.
        fig.text(x_text, y_text, name, va='center', ha='left', fontsize=10)

    fig.savefig('my-gradients.pdf')
def toname(g):
    return g.__name__.replace('gradient_', '').replace('_', '-').upper()
    
gradients = (gradient_rgb_bw, gradient_rgb_gbr, gradient_rgb_gbr_full, gradient_rgb_wb_custom,
                 gradient_hsv_bw, gradient_hsv_gbr, gradient_hsv_unknown, gradient_hsv_custom)

plot_color_gradients(gradients, [toname(g) for g in gradients])